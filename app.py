from flask import Flask, render_template, request, redirect, url_for
import json
app = Flask(__name__)

def doInference(input):
    return redirect(url_for('result', result = json.dumps(input)))

@app.route("/", methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        input = []
        input['price'] = request.form['price']
        input['location'] = request.form['location']
        input['facility'] = request.form['facility']
        return doInference(input)
    else:
        return render_template("home.html")

@app.route("/result")
def result():
    result = request.args['result']
    return render_template('result.html', result = json.loads(result))